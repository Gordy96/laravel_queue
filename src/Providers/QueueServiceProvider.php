<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 19.10.17
 * Time: 11:06
 */

namespace Johnny\Queue\Providers;

use Illuminate\Queue\QueueServiceProvider as DefaultProvider;
use Johnny\Queue\Connectors\DatabaseConnector;

class QueueServiceProvider extends DefaultProvider
{
    /**
     * Register the database queue connector.
     *
     * @param  \Illuminate\Queue\QueueManager  $manager
     * @return void
     */
    protected function registerDatabaseConnector($manager)
    {
        $manager->addConnector('database', function () {
            return new DatabaseConnector($this->app['db']);
        });
    }
}