<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 19.10.17
 * Time: 10:59
 */

namespace Johnny\Queue\Connectors;

use Illuminate\Queue\Connectors\DatabaseConnector as DefaultConnector;
use Johnny\Queue\DatabaseQueue;

class DatabaseConnector extends DefaultConnector
{
    /**
     * Establish a queue connection.
     *
     * @param  array  $config
     * @return \Illuminate\Contracts\Queue\Queue
     */
    public function connect(array $config)
    {
        return new DatabaseQueue(
            $this->connections->connection($config['connection'] ?? null),
            $config['table'],
            $config['queue'],
            $config['retry_after'] ?? 60
        );
    }
}