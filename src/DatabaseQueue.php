<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 19.10.17
 * Time: 11:03
 */

namespace Johnny\Queue;

use Illuminate\Queue\DatabaseQueue as DefaultQueue;
use Illuminate\Queue\Jobs\DatabaseJobRecord;
use Illuminate\Support\Facades\DB;

class DatabaseQueue extends DefaultQueue
{
    /**
     * Get the next available job for the queue.
     *
     * @param  string|null  $queue
     * @return \Illuminate\Queue\Jobs\DatabaseJobRecord|null
     */
    protected function getNextAvailableJob($queue)
    {
        $query = $this->database->table($this->table)
            ->lockForUpdate();
        if(!is_null($queue) && strpos($queue, "*") !== FALSE){
            $query = $query->where('queue', 'LIKE', str_ireplace("*","%", $queue));
        }
        else{
            $query = $query->where('queue', $this->getQueue($queue));
        }
        $job = $query->where(function ($query) {
                $this->isAvailable($query);
                $this->isReservedButExpired($query);
            })
            ->orderBy('id', 'asc')
            ->first();

        return $job ? new DatabaseJobRecord((object) $job) : null;
    }
    public static function killJob($id){
        DB::table('jobs')->delete($id);
    }
}